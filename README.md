# Dev Setup
1. Make sure golang is installed.
2. Install hugo
`brew install hugo`
[Hugo](https://gohugo.io/)

## Run hugo server
`hugo server -D`

# Current Theme
[Osprey] (https://themes.gohugo.io/osprey/)
